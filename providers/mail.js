const nodemailer = require("nodemailer");

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
    host: "smtp.ethereal.email",
    port: 587,
    auth: {
        user: "amparo63@ethereal.email",
        pass: "ZktTAA2P2FwF2N2EC8",
    },
});

const mailOptionsDefault = {
  from: '"Fred Foo 👻" <foo@example.com>', // sender address
  to: "bar@example.com, baz@example.com", // list of receivers
  subject: "Hello ✔", // Subject line
  text: "Hello world?", // plain text body
  html: "<b>Hello world?</b>", // html body
}

// async..await is not allowed in global scope, must use a wrapper
async function sendMail(mailOptions = mailOptionsDefault) {

  // send mail with defined transport object
  let info = await transporter.sendMail(mailOptions);

  return {
      messageId: info.messageId,
      previewUrl: nodemailer.getTestMessageUrl(info),
  };
}

module.exports = sendMail