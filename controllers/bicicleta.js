const Bicicleta = require('../models/bicicleta')
const { fnBicicletaCreate } = require('./api/bicicletaController')

exports.bicicletaList = function(req, res){
    Bicicleta.allBicis(function(err, bicicletas){
        console.log("bicicletas", bicicletas);
        res.render("bicicletas/index", { bicis: bicicletas });
    })
}

exports.bicicletaCreateView = function(req, res){
    res.render('bicicletas/create')
}

exports.bicicletaCreate = function(req, res){

    fnBicicletaCreate(req.body)

    res.redirect('/bicicletas')

}

exports.bicicletaUpdateView = async function (req, res) {

    const bici = await  Bicicleta.findById(req.params.id)

    res.render('bicicletas/update', { bici })
}

exports.bicicletaUpdate = function (req, res) {

    const aBici = Bicicleta.findById(req.params.id)
    const { color, modelo, lat, lng } = req.body

    aBici.color = color
    aBici.modelo = modelo
    aBici.ubicacion = [lat, lng]

    res.redirect('/bicicletas')

}

exports.bicicletaRemove = function(req, res){

    Bicicleta.removeById(req.params.id)
    res.redirect('/bicicletas')

}



