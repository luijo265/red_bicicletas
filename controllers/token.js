const Usuario = require('../models/usuario')
const Token = require('../models/token')

const confirmationGet = async function(req, res, next){
    const token = await Token.findOne({ token: req.params.token })

    if (!token) return res.status(400).send({ msg: 'No encontramos un usuario con este token. Quizá haya expirado y debas solicitar otro' })

    try {
        const usuario = await Usuario.findById(token.usuario)
        if (!usuario) return res.status(400).send({ msg: 'No encontramos un usuario para este token.' })
        if( usuario.verificado ) return res.redirect('/usuarios')
        usuario.verificado = true
        await usuario.save()
        res.redirect('/')
    } catch (error) {
        res.status(500).send({ msg: error.message })
        res.redirect('/')
    }

}

module.exports = {
    confirmationGet
}