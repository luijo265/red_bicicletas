const Usuario = require("../../models/usuario");

const usuarioList = async (req, res) => {

    const users = await Usuario.find({})

    res.status(200).json({
        users
    });
};

const usuarioCreate = async (req, res) => {

    const { nombre } = req.body

    const user = new Usuario({nombre})
    user.save()

    res.status(200).json({
        user
    });
};

const usuarioUpdate = async (req, res) => {

    const _id = req.params.id
    const { nombre } = req.body

    const user = await Usuario.findById(_id)
    
    user.nombre = nombre
    user.save()

    res.status(200).json({
        user
    });
};

const usuarioRemove = async (req, res) => {
  const _id = req.params.id;
  
  const user = await Usuario.removeById(_id)

  res.status(200).json({
    user,
  });
};

module.exports = {
  usuarioList,
  usuarioCreate,
  usuarioUpdate,
  usuarioRemove,
};