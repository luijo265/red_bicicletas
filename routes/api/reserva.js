const express = require("express");
const router = express.Router();
const reservaController = require("../../controllers/api/reservaController");

router.get("/", reservaController.reservaList);
router.post("/", reservaController.reservaCreate);
router.put("/:id", reservaController.reservaUpdate);
router.delete("/:id", reservaController.reservaRemove);
router.post("/send_email", reservaController.sendEmail);

module.exports = router;
