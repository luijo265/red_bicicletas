const express = require("express");
const router = express.Router();
const usuarioController = require("../../controllers/api/usuarioController");

router.get("/", usuarioController.usuarioList);
router.post("/", usuarioController.usuarioCreate);
router.put("/:id", usuarioController.usuarioUpdate);
router.delete("/:id", usuarioController.usuarioRemove);

module.exports = router;
