const express = require('express')
const router = express.Router()
const bicicletaController = require('../../controllers/api/bicicletaController')

router.get('/', bicicletaController.bicicletaList)
router.post('/', bicicletaController.bicicletaCreate)
router.put('/:id', bicicletaController.bicicletaUpdate)
router.delete('/:id', bicicletaController.bicicletaRemove)

module.exports = router