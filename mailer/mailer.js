const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");

let mailConfig;
if (process.env.NODE_ENV == 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
}

if (process.env.NODE_ENV == 'staging') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
}

if (process.env.NODE_ENV == "development") {
    mailConfig = {
        host: "smtp.ethereal.email",
        port: 587,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PW,
        },
    };
}

module.exports = nodemailer.createTransport(mailConfig);
