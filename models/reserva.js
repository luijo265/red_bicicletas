const mongoose = require('mongoose')
const Schema = mongoose.Schema
const moment = require('moment')

const reservaSchema = new Schema({
    desde: Date,
    hasta: Date,
    bicicleta: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'
    },
    usuario: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'
    }
})

reservaSchema.methods.diasDeReserva = function(){
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1
}

reservaSchema.statics.removeById = function (_id) {
  return this.deleteOne({ _id });
};

reservaSchema.statics.allReserva = async function () {
  const reservas = await this.find({})
      .populate("usuario")
      .populate("bicicleta");

    return reservas.map(item => {
        return {
          desde: moment(item.desde).format("YYYY-MM-DD"),
          hasta: moment(item.hasta).format("YYYY-MM-DD"),
          usuario: item.usuario.nombre,
          modelo: item.bicicleta.modelo,
          color: item.bicicleta.color,
          codigo: item.bicicleta.code,
          dia_reserva: item.diasDeReserva(),
        };
    })
};

module.exports = mongoose.model('Reserva', reservaSchema)