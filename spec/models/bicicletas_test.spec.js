const mongoose = require('mongoose')
const Bicicleta = require('../../models/bicicleta')

describe('Testing Bicicltas', function(){

    beforeAll(function(done){
        // const mongoDB = "mongodb://coursera:123456@localhost/coursera";
        const mongoDB = "mongodb://localhost/red_bicicletas";
        mongoose.connect(mongoDB, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            autoIndex:false
        });
        mongoose.Promise = global.Promise
        const db = mongoose.connection
        db.on('error', console.error.bind('MongoDB connection error'))
        db.on('open', function(){
            console.log('We are connected to test database!')
            done()
        })
    })

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err)
            done()
        })
    })

    describe('Bicicleta create.instance', () => {
        it('Crea una instancia de Bicicleta', () => {

            const bici = Bicicleta.createInstance(1, 'Verde', 'BMX', [-34.5, 54.1])

            expect(bici.code).toBe(1)
            expect(bici.color).toBe('Verde')
            expect(bici.modelo).toBe('BMX')
            expect(bici.ubicacion[0]).toBe(-34.5)
            expect(bici.ubicacion[1]).toBe(54.1)

            // console.log('Finalizado crear instancia')

        })
    })


    describe('Bicicletas.allBicis', () => {
        it('comienza vacia', done => {

            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0)
                // console.log('finalizado 0 bicis')
                done()
            })

        })
    })

    describe('Bicicletas.add', () => {
        it('Agregamos una bicicleta', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0)

                Bicicleta.add({ 
                    code:2, 
                    color:'verde', 
                    modelo:'urbana', 
                    ubicacion: [10.4589894, -66.5325691]
                }, function(err, newBici){
                    if (err) {
                        console.log(err)
                        done()
                    }
                    Bicicleta.allBicis(function(err, bici){
                        expect(bici.length).toBe(1)
                        // console.log('Finalizado agregar bicicleta')
                        done()
                    })
                })
                
            })

        })
    })


    describe('Bicicletas.findByID', () => {
        it('debe devolver bici con id 1', async () => {

            let allBicis = await Bicicleta.allBicis

            expect(allBicis.length).toBe(0)

            await Bicicleta.add({
                code: 1, 
                color: 'azul', 
                modelo: 'urbana', 
                ubicacion: [10.4589894, -66.5325691]
            })
            await Bicicleta.add({
                code: 2, 
                color: 'verde', 
                modelo: 'urbana', 
                ubicacion: [10.4589894, -66.5325691]
            })
         

            const targetBici = await Bicicleta.findByCode(1)
            expect(targetBici.code).toBe(1)
            expect(targetBici.color).toBe('azul')
            expect(targetBici.modelo).toBe('urbana')
        })
    })

})

/*
beforeEach(() => {
    Bicicleta.allBicis = []
})

describe('Bicicletas.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    })
})

describe('Bicicletas.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        let b = new Bicicleta(2, 'verde', 'urbana', [10.4589894, -66.5325691])
        Bicicleta.add(b)

        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(b)
    })
})

describe('Bicicletas.findByID', () => {
    it('debe devolver bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        const aBici = new Bicicleta(1, 'azul', 'urbana', [10.4589894, -66.5325691])
        const aBici2 = new Bicicleta(2, 'verde', 'urbana', [10.4589894, -66.5325691])
        Bicicleta.add(aBici)
        Bicicleta.add(aBici2)

        const targetBici = Bicicleta.findById(1)
        expect(targetBici.id).toBe(1)
        expect(targetBici.color).toBe(aBici.color)
        expect(targetBici.modelo).toBe(aBici.modelo)
    })
})

describe('Bicicletas.removeById', () => {
    it('Remover 1 bici', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        const aBici = new Bicicleta(1, 'azul', 'urbana', [10.4589894, -66.5325691])
        const aBici2 = new Bicicleta(2, 'verde', 'urbana', [10.4589894, -66.5325691])
        Bicicleta.add(aBici)
        Bicicleta.add(aBici2)

        expect(Bicicleta.allBicis.length).toBe(2)
        Bicicleta.removeById(1)
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(() => Bicicleta.findById(1)).toThrow(new Error(`No existe una bicicleta con el id 1`))
        
    })
})

describe('Bicicletas.toString', () => {
    it('Obtener string de bicicleta', () => {
        expect(Bicicleta.allBicis.length).toBe(0)
        const aBici = new Bicicleta(1, 'azul', 'urbana', [10.4589894, -66.5325691])
        Bicicleta.add(aBici)

        expect(Bicicleta.findById(1).toString()).toBe(`id: ${aBici.id} | color: ${aBici.color}`)

    })
})

*/