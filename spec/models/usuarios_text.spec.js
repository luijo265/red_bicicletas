const mongoose = require("mongoose");
const Reserva = require("../../models/reserva");
const Bicicleta = require("../../models/bicicleta");
const Usuario = require("../../models/usuario");

describe('Testing Usuarios', function(){

    beforeAll(function (done) {
      // const mongoDB = "mongodb://coursera:123456@localhost/coursera";
      const mongoDB = "mongodb://localhost/red_bicicletas";
      mongoose.connect(mongoDB, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        autoIndex: false,
      });
      mongoose.Promise = global.Promise;
      const db = mongoose.connection;
      db.on("error", console.error.bind("MongoDB connection error"));
      db.on("open", function () {
        console.log("We are connected to test database!");
        done();
      });
    });

    afterEach(async () => {
        await Reserva.deleteMany({})
        await Usuario.deleteMany({})
        await Bicicleta.deleteMany({})
    });

    describe('Cuando un usuario reserva una bici', () => {
        it('debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre:'Luijo'})
            usuario.save()

            const bicicleta = new Bicicleta({
              code: 1,
              color: 'Rojo',
              modelo: 'BMX',
              ubicacion: [-34.5, 54.1],
            });
            bicicleta.save()

            const hoy = new Date()
            const manana = new Date()

            manana.setDate(hoy.getDate() + 1)
            usuario.reservar(bicicleta._id, hoy, manana, function(err, reserva) {

                Reserva.find({})
                  .populate("usuario")
                  .populate("bicicleta")
                  .exec(function (err, reservas) {

                    // console.log('consulta', reservas);
                    const reservaDb = reservas[0];

                    // console.log(".length", reservas.length);
                    expect(reservas.length).toBe(1)
                    // console.log(".diasDeReserva()", reservaDb.diasDeReserva());
                    expect(reservaDb.diasDeReserva()).toBe(2);
                    expect(reservaDb.bicicleta.code).toBe(1);
                    expect(reservaDb.usuario.nombre).toBe("Luijo");
                    done()

                  });

            });

        })
        
    })
    

})